from os.path import join

from pythonforandroid.recipe import PythonRecipe, warning

from pythonforandroid.recipe import CompiledComponentsPythonRecipe
from pythonforandroid.toolchain import current_directory



class Pygame_sdl2Recipe(CompiledComponentsPythonRecipe):

    #version = '007e03348dbd8d3de3eb09022d72c734a8608144'
    # Moved to github.com instead of ffmpeg.org to improve download speed
    #url = 'https://https://github.com/renpy/pygame_sdl2/archive/{version}.zip'

    name = 'pygame_sdl2'
    version = '2.1.0'
    #url = 'https://testpypi.python.org/packages/2f/f1/8dd343ae2d97c432cf1c81ca03d2d0472396f9d8e76c677aeae9ce2ec61d/pygame_sdl2-{version}.tar.gz'
    url = 'https://nightly.renpy.org/current/pygame_sdl2-2.1.0-for-renpy-nightly-2021-03-19-50aedffdd.tar.gz'

    site_packages_name= 'pygame_sdl2'

    #depends = []

    depends = ['sdl2', 'sdl2_image', 'sdl2_mixer', 'sdl2_ttf', 'setuptools', 'jpeg', 'png']
    call_hostpython_via_targetpython = False  # Due to setuptools
    install_in_hostpython = False

    def prebuild_arch(self, arch):
        super().prebuild_arch(arch)

    def include_flags(self, arch):
        '''Returns a string with the include folders'''

        png = self.get_recipe('png', self.ctx)
        png_lib_dir = join(png.get_build_dir(arch.arch), '.libs')
        png_inc_dir = png.get_build_dir(arch)

        jpeg = self.get_recipe('jpeg', self.ctx)
        jpeg_inc_dir = jpeg_lib_dir = jpeg.get_build_dir(arch.arch)

        sdl_ttf_includes= join(self.ctx.bootstrap.build_dir, 'jni', 'SDL2_ttf')
        sdl_image_includes= join(self.ctx.bootstrap.build_dir, 'jni', 'SDL2_image')
        sdl_mixer_includes= join(self.ctx.bootstrap.build_dir, 'jni', 'SDL2_mixer')

        sdl2_includes = join(self.ctx.bootstrap.build_dir, 'jni', 'SDL', 'include')

        return (' -I' + sdl2_includes + 
                ' -I' + sdl_ttf_includes + 
                ' -I' + sdl_image_includes + 
                ' -I' + sdl_mixer_includes +
                ' -I' + jpeg_inc_dir + 
                ' -I' + png_inc_dir)

    def link_dirs_flags(self, arch):

        png = self.get_recipe('png', self.ctx)
        png_lib_dir = join(png.get_build_dir(arch.arch), '.libs')

        jpeg = self.get_recipe('jpeg', self.ctx)
        jpeg_lib_dir = jpeg.get_build_dir(arch.arch)

        ndk_lib_dir = join(self.ctx.ndk_platform, 'usr', 'lib')

        return (
            ' -L' + join(self.ctx.bootstrap.build_dir, "libs", str(arch)) + 
            ' -L' + png_lib_dir +
            ' -L' + jpeg_lib_dir +
            ' -L' + ndk_lib_dir)

    # def prebuild_arch(self, arch):
    #     super(Pygame_sdl2Recipe, self).prebuild_arch(arch)
    #     # AND: Fix this warning!
    #     warning('Pygame_sdl2 is built assuming the archiver name is '
    #         'arm-linux-androideabi-ar, which may not always be true!')

    # def prebuild_arch(self, arch):
    #     super().prebuild_arch(arch)
    #     with current_directory(self.get_build_dir(arch.arch)):
    #         setup_template = open(join("buildconfig", "Setup.Android.SDL2.in")).read()
    #         env = self.get_recipe_env(arch)
    #         env['ANDROID_ROOT'] = join(self.ctx.ndk_platform, 'usr')

    #         ndk_lib_dir = join(self.ctx.ndk_platform, 'usr', 'lib')

    #         png = self.get_recipe('png', self.ctx)
    #         png_lib_dir = join(png.get_build_dir(arch.arch), '.libs')
    #         png_inc_dir = png.get_build_dir(arch)

    #         jpeg = self.get_recipe('jpeg', self.ctx)
    #         jpeg_inc_dir = jpeg_lib_dir = jpeg.get_build_dir(arch.arch)

    #         setup_file = setup_template.format(
    #             sdl_includes=(
    #                 " -I" + join(self.ctx.bootstrap.build_dir, 'jni', 'SDL', 'include') +
    #                 " -L" + join(self.ctx.bootstrap.build_dir, "libs", str(arch)) +
    #                 " -L" + png_lib_dir + " -L" + jpeg_lib_dir + " -L" + ndk_lib_dir),
    #             sdl_ttf_includes="-I"+join(self.ctx.bootstrap.build_dir, 'jni', 'SDL2_ttf'),
    #             sdl_image_includes="-I"+join(self.ctx.bootstrap.build_dir, 'jni', 'SDL2_image'),
    #             sdl_mixer_includes="-I"+join(self.ctx.bootstrap.build_dir, 'jni', 'SDL2_mixer'),
    #             jpeg_includes="-I"+jpeg_inc_dir,
    #             png_includes="-I"+png_inc_dir,
    #             freetype_includes=""
    #         )
    #         open("Setup", "w").write(setup_file)


    def get_recipe_env(self, arch):
        env = super().get_recipe_env(arch)
        env["PYGAME_SDL2_ANDROID"] = "TRUE"
        env['CFLAGS'] += self.include_flags(arch)
        env['LDFLAGS'] += self.link_dirs_flags(arch)

        env['USE_SDL2'] = '1'
        env["PYGAME_CROSS_COMPILE"] = "TRUE"
        env["PYGAME_ANDROID"] = "TRUE"
        return env

recipe = Pygame_sdl2Recipe()
