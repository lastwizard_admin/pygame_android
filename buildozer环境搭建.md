======================

buildozer 环境

======================


1. 安装buildozer docker环境
    ```
    git clone https://github.com/kivy/buildozer.git
    docker build --tag=buildozer .
    ```

2. 启动docker, 并进入docker系统
   ```
   docker run -d --name buildozer -it -v "$PWD":/home/user/hostcwd -v /Users/leo/Documents/workspace/kivy/FlappyKivy:/home/user/apk -v /Users/leo/Downloads/sdk/buildozer_env:/home/user/.buildozer --entrypoint /bin/bash buildozer

   docker exec -it buildozer /bin/bash
   ```

3. 安装环境，防止aidl not find（ubuntu20），需要在 `buildozer.spec`设置以下参数，自动下载更新
    `android.skip_update = False`

    或者手动下载
    ```
        sudo apt update
        sudo apt install -y git zip unzip openjdk-8-jdk python3-pip autoconf libtool pkg-config zlib1g-dev libncurses5-dev libncursesw5-dev libtinfo5 cmake libffi-dev libssl-dev
        pip3 install --user --upgrade Cython==0.29.19 virtualenv  # the --user should be removed if you do this in a venv

        # add the following line at the end of your ~/.bashrc file
        export PATH=$PATH:~/.local/bin/

        ~/.buildozer/android/platform/android-sdk/tools/bin/sdkmanager "build-tools;29.0.0" --sdk_root=/home/user/.buildozer/android/platform/android-sdk

    ```
    注意需要--sdk_root参数，否则会出现以下错误：
    ```
    Warning: Could not create settings
    java.lang.IllegalArgumentException
    ```
5.  编译
    ` buildozer -V android debug `


6. adb remote debug

    ` adb tcpip 5555`
    ` adb connect 10.0.0.11:5555`
     