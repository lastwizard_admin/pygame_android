# pygame for android 

#### 介绍
基于pygame实现的flappybird游戏，通过python-for-android移植到android系统

**本项目主要用作抛砖引玉，可以参考本项目移植各种pygame游戏到android、ios平台** 
#### 一些废话 
-------------
    将pygame移植到android还是挺费劲的，
    尝试了kivy、rapt-renpy、python-for-android自带项目，
    均不能正常运行（闪退），最后从qpython项目找到相关思路，
    采用pygame_sdl2（感谢renpy）添加到p4a里面，直接使用buildozer打包，
    完美实现pygame项目在android里面的运行。
#### 一些坑
-------------
    在android里面不能直接使用pygame的刷新，不然刷新会很慢很慢。。。
    需要缓存render等，参考本项目
    另外触屏和pygame事件的对应还没搞定，后续再研究


#### 软件架构
1. python-for-android 
2. pygame_sdl2 
3. buildozer


#### 安装教程
1. 搭建buildozer环境
2. 在python-for-android recipes里面增加pygame_sdl2(可将本项目recipes里面的pygame_sdl2拷贝到python-for-android相应目录)
3. buildozer.spec requirments里面增加 cython,pygame_sdl2
4. buildozer打包

#### 使用说明

1.  参考buildozer编译，生成apk

```
    buildozer -V android debug deploy
```

#### 可能依赖仓库

1.  [buildozer](https://gitee.com/lastwizard_admin/buildozer.git)
2.  [python-for-android (自动下载)](https://gitee.com/lastwizard_admin/python-for-android.git)
3.  [pygame_sdl2](https://github.com/renpy/pygame_sdl2.git)

#### buildozer 环境搭建


1. 安装buildozer docker环境
    ```
    git clone https://github.com/kivy/buildozer.git
    docker build --tag=buildozer .
    ```

2. 启动docker, 并进入docker系统
   ```
   docker run -d --name buildozer -it -v "$PWD":/home/user/hostcwd -v /Users/leo/Documents/workspace/kivy/FlappyKivy:/home/user/apk -v /Users/leo/Downloads/sdk/buildozer_env:/home/user/.buildozer --entrypoint /bin/bash buildozer

   docker exec -it buildozer /bin/bash
   ```

3. 安装环境，防止aidl not find（ubuntu20），需要在 `buildozer.spec`设置以下参数，自动下载更新
    `android.skip_update = False`

    或者手动下载
    ```
        sudo apt update
        sudo apt install -y git zip unzip openjdk-8-jdk python3-pip autoconf libtool pkg-config zlib1g-dev libncurses5-dev libncursesw5-dev libtinfo5 cmake libffi-dev libssl-dev
        pip3 install --user --upgrade Cython==0.29.19 virtualenv  # the --user should be removed if you do this in a venv

        # add the following line at the end of your ~/.bashrc file
        export PATH=$PATH:~/.local/bin/

        ~/.buildozer/android/platform/android-sdk/tools/bin/sdkmanager "build-tools;29.0.0" --sdk_root=/home/user/.buildozer/android/platform/android-sdk

    ```
    注意需要--sdk_root参数，否则会出现以下错误：
    ```
    Warning: Could not create settings
    java.lang.IllegalArgumentException
    ```
5.  编译
    ` buildozer -V android debug `


6. adb remote debug

    ` adb tcpip 5555`
    ` adb connect 10.0.0.11:5555`